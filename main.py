import pandas as pd
from fpdf import FPDF
import glob
from pathlib import Path
from datetime import datetime

filepaths = glob.glob("invoices/*.xlsx")


for filepath in filepaths:

    pdf = FPDF(orientation="P", unit="mm", format="A4")
    pdf.add_page()

    filename = Path(filepath).stem
    invoiceNr, date = filename.split('-')
    date = datetime.strptime(date, '%Y.%m.%d')

    pdf.set_font(family="Times", size=16, style='B')
    pdf.cell(w=50, h=8, txt=f"Invoice nr.{invoiceNr}", ln=1)

    pdf.set_font(family="Times", size=16, style='B')
    pdf.cell(w=50, h=8, txt=f"Date: {date.strftime('%d.%m.%Y')}", ln=1)
    pdf.ln()

    df = pd.read_excel(filepath, sheet_name="Sheet 1")
    headers = df.columns
    headers = [header.replace('_',' ').title() for header in headers]

    pdf.set_font(family="Times", size=10, style='B')
    pdf.set_text_color(80, 80, 80)
    pdf.cell(w=30, h=8, txt=str(headers[0]), border=1)
    pdf.cell(w=70, h=8, txt=str(headers[1]), border=1)
    pdf.cell(w=30, h=8, txt=str(headers[2]), border=1)
    pdf.cell(w=30, h=8, txt=str(headers[3]), border=1)
    pdf.cell(w=30, h=8, txt=str(headers[4]), border=1)
    pdf.ln()

    totalSum = df["total_price"].sum()
    for index, row in df.iterrows():
        pdf.set_font(family="Times", size=10)
        pdf.set_text_color(80,80,80)
        pdf.cell(w=30, h=8, txt=str(row["product_id"]), border=1)
        pdf.cell(w=70, h=8, txt=str(row["product_name"]), border=1)
        pdf.cell(w=30, h=8, txt=str(row["amount_purchased"]), border=1)
        pdf.cell(w=30, h=8, txt=str(row["price_per_unit"]), border=1)
        pdf.cell(w=30, h=8, txt=str(row["total_price"]), border=1)
        pdf.ln()

    pdf.set_font(family="Times", size=10)
    pdf.set_text_color(80, 80, 80)
    pdf.cell(w=0, h=8, txt=f"Total Price: {str(totalSum)}", border=1, align='C', ln=1)

    pdf.ln()

    pdf.set_font(family="Times", size=12, style='B')
    pdf.set_text_color(80, 80, 80)
    pdf.cell(w=30, h=8, txt=f"The total price is {totalSum}", ln=1)

    # pdf.cell(w=30, h=8, txt="Company name")
    # pdf.image("image.png", w=10)


    pdf.output(f"pdfs/{invoiceNr}.pdf")